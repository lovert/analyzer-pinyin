##SOLR 4.7.2插件使用详情
	
#solr schema.xml的配置信息
```xml
	<!---->
		<fieldType name="text_pinyin" class="solr.TextField" positionIncrementGap="100">
	      <analyzer type="index">
		<tokenizer class="org.apache.lucene.analysis.ik.IKTokenizerFactory" useSmart="false"/>
		<filter class="org.apache.lucene.analysis.pinyin.solr4.PinyinTokenFilterFactory" pinyinAll="false" shortPinyin="true"  minTermLength="1" />
		<!-- in this example, we will only use synonyms at query time
		<filter class="solr.SynonymFilterFactory" synonyms="index_synonyms.txt" ignoreCase="true" expand="false"/>
		-->
			<!--
		<filter class="org.apache.lucene.analysis.pinyin.solr4.PinyinNGramTokenFilterFactory" nGramChinese="true" nGramNumber="true"/>
			-->
	      </analyzer>
		  
	      <analyzer type="query">
		<tokenizer class="org.apache.lucene.analysis.ik.IKTokenizerFactory" useSmart="false"/>
		<filter class="org.apache.lucene.analysis.pinyin.solr4.PinyinTokenFilterFactory" pinyinAll="false"  shortPinyin="true" minTermLength="1" />
		<!-- in this example, we will only use synonyms at query time
		<filter class="solr.SynonymFilterFactory" synonyms="index_synonyms.txt" ignoreCase="true" expand="false"/>
		-->
			<!--
		<filter class="org.apache.lucene.analysis.pinyin.solr4.PinyinNGramTokenFilterFactory" nGramChinese="true" nGramNumber="true"/>
			-->
	      </analyzer>
	    </fieldType>
```
##配置参数含义

	shortPinyin是否启用简拼 默认不用简拼 值为false 如果启用简拼要设置pinyinAll的值为false
	pinyinAll是否启用全拼+简拼 默认 是全拼加简拼
	minTermLength汉字最小转换拼音长度 默认长度为2
	isContainerNumber 如果分词中包含数字，是否拼音中也包含数字 默认值为包含
	outChinese 是否输出汉字 默认输出true