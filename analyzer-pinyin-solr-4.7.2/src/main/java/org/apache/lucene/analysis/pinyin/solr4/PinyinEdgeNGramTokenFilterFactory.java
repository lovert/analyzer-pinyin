package org.apache.lucene.analysis.pinyin.solr4;

import java.util.Map;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.pinyin.lucene4.PinyinEdgeNGramTokenFilter;
import org.apache.lucene.analysis.util.TokenFilterFactory;

/**
 * 对转换后的拼音进行EdgeNGram处理的TokenFilterFactory
 * @author 殷龙飞
 */
public class PinyinEdgeNGramTokenFilterFactory extends TokenFilterFactory
{
  private int minGram;
  private int maxGram;
  private String side;
  /**
   * 是否需要对中文进行NGram[默认为false]
   */
  private boolean nGramChinese; 
  /**
   * 
   */
  private boolean nGramNumber;

  public PinyinEdgeNGramTokenFilterFactory(Map<String, String> args)
  {
    super(args);

    this.minGram = getInt(args, "minGram", 2);
    this.maxGram = getInt(args, "maxGram", 10);
    this.side = get(args, "side", "front");
    this.nGramChinese = getBoolean(args, "nGramChinese", false);
    this.nGramNumber = getBoolean(args, "nGramNumber", false);
  }

  public TokenFilter create(TokenStream input) {
    return new PinyinEdgeNGramTokenFilter(input, this.side, this.minGram, this.maxGram, 
      this.nGramChinese, this.nGramNumber);
  }
}