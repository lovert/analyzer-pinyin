package org.apache.lucene.analysis.pinyin.solr4;

import java.util.Map;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.pinyin.lucene4.PinyinTokenFilter;
import org.apache.lucene.analysis.util.TokenFilterFactory;
/**
 * 
 * @author 殷龙飞
 */
public class PinyinTokenFilterFactory extends TokenFilterFactory
{
  private boolean outChinese;
  private boolean shortPinyin;
  private boolean pinyinAll;
  private int minTermLength;
  private boolean isContainerNumber;
  

  public PinyinTokenFilterFactory(Map<String, String> args)
  {
    super(args);
    this.outChinese = getBoolean(args, "outChinese", true);
    this.shortPinyin = getBoolean(args, "shortPinyin", false);
    this.pinyinAll = getBoolean(args, "pinyinAll", true);
    this.minTermLength = getInt(args, "minTermLength", 2);
    this.isContainerNumber = getBoolean(args, "isContainerNumber", true);
  }

  public TokenFilter create(TokenStream input) {
    return new PinyinTokenFilter(input, this.shortPinyin, this.pinyinAll, this.outChinese,this.minTermLength,this.isContainerNumber);
  }

  public boolean isOutChinese() {
    return this.outChinese;
  }

  public void setOutChinese(boolean outChinese) {
    this.outChinese = outChinese;
  }

  public boolean isShortPinyin() {
    return this.shortPinyin;
  }

  public void setShortPinyin(boolean shortPinyin) {
    this.shortPinyin = shortPinyin;
  }

  public boolean isPinyinAll() {
    return this.pinyinAll;
  }

  public void setPinyinAll(boolean pinyinAll) {
    this.pinyinAll = pinyinAll;
  }

  public int getMinTermLength()
  {
    return this.minTermLength;
  }

  public void setMinTermLength(int minTermLength) {
    this.minTermLength = minTermLength;
  }
}