package org.apache.lucene.analysis.pinyin.solr4;

import java.util.Map;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.pinyin.lucene4.PinyinNGramTokenFilter;
import org.apache.lucene.analysis.util.TokenFilterFactory;

/**
 * 
 * @author 殷龙飞
 */
public class PinyinNGramTokenFilterFactory extends TokenFilterFactory
{
  private int minGram;
  private int maxGram;
  private boolean nGramChinese;
  private boolean nGramNumber;

  public PinyinNGramTokenFilterFactory(Map<String, String> args)
  {
    super(args);

    this.minGram = getInt(args, "minGram", 2);
    this.maxGram = getInt(args, "maxGram", 10);
    this.nGramChinese = getBoolean(args, "nGramChinese", false);
    this.nGramNumber = getBoolean(args, "nGramNumber", false);
  }

  public TokenFilter create(TokenStream input) {
    return new PinyinNGramTokenFilter(input, this.minGram, this.maxGram, 
      this.nGramChinese, this.nGramNumber);
  }
}