package org.apache.lucene.analysis.ik;

import java.io.Reader;
import java.util.Map;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeSource.AttributeFactory;
import org.wltea.analyzer.lucene.IKTokenizer;
/**
 * 
 * @author 殷龙飞
 *
 */
public class IKTokenizerFactory extends TokenizerFactory {
	//是否智能分词
	private boolean useSmart;

	public IKTokenizerFactory(Map<String, String> args) {
		super(args);
		this.useSmart = getBoolean(args, "useSmart", false);
	}

	@Override
	public Tokenizer create(AttributeFactory attributefactory, Reader reader) {
		Tokenizer tokenizer = new IKTokenizer(reader, this.useSmart);
		
		//org.apache.lucene.analysis.ik.IKTokenizerFactory;
		//org.apache.lucene.analysis.pinyin.solr4.PinyinTokenFilterFactory
		return tokenizer;
	}

	// public Tokenizer create(AttributeFactory attributeFactory)
	// {
	// Tokenizer tokenizer = new IKTokenizer(attributeFactory, this.useSmart);
	// return tokenizer;
	// }
}