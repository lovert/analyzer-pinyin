package org.apache.lucene.analysis.pinyin.utils;
/**
 * 字符串工具类
 * @author 殷龙飞
 */
public class StringUtils {
	/**
	 * 字符串中是否包含汉字
	 * @param s
	 * @return
	 */
	public static boolean containsChinese(String s) {
		if ((s == null) || ("".equals(s.trim())))
			return false;

		for (int i = 0; i < s.length(); ++i)
			if (isChinese(s.charAt(i)))
				return true;

		return false;
	}
	/**
	 * 字符是否是汉字
	 * @param a
	 * @return
	 */
	public static boolean isChinese(char a) {
		int v = a;
		return ((v >= 19968) && (v <= 171941));
	}
	
	/**
	 * 字符串是否是汉字
	 * @param strName
	 * @return
	 */
	public static boolean isChinese(String strName) {
		char[] ch = strName.toCharArray();
		for (int i = 0; i < ch.length; ++i) {
			char c = ch[i];
			if (!(isChinese(c)))
				return false;
		}

		return true;
	}
	/**
	 * 字符串中包含的汉字字符的个数
	 * @param s
	 * @return
	 */
	public static int chineseCharCount(String s) {
		int count = 0;
		if ((s == null) || ("".equals(s.trim())))
			return count;
		for (int i = 0; i < s.length(); ++i)
			if (isChinese(s.charAt(i)))
				++count;

		return count;
	}
	/**
	 * 字符串中是否包含数字
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		boolean flag = false;
		if ((str == null) || (str.length() == 0)) {
			flag = false;

		} else {

			for (int i = str.length(); --i >= 0;) {
				if (Character.isDigit(str.charAt(i))) {
					flag = true;
				}
			}
		}

		return flag;
	}
}